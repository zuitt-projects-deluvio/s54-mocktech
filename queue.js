let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
  return collection
}

// Adds element to the rear of the queue
function enqueue(element) {
  let res = collection.push(element)
  return collection
} 

// Removes element from the front of the queue
function dequeue() {
  let res = collection.shift()
  return collection
}

// Show element at the front
function front(element) {
    let frontIndex = collection[0]
    return frontIndex
}

// Show total number of elements
function size() {
   return collection.length
}

// Outputs a Boolean value describing whether queue is empty or not
function isEmpty() {
    return collection.length !== 0 ? false : true
}

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};